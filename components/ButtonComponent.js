import React from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'
import { SimpleLineIcons} from '@expo/vector-icons';

const ButtonComponent = (props) => {
    return (
            <TouchableOpacity 
                testID={'buttonComponent'}
                style={styles.buttons} 
                onPress={props.onPress} 
            >
               { props.icon }
                <Text testID={'buttonTitle'} style={styles.buttonsText}>{props.title}</Text>
            </TouchableOpacity>
    )
}

export default ButtonComponent

const styles = StyleSheet.create({
    buttons: {
        width: '50%',
        marginVertical: 5,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#9cf0ff',
        padding: 10,
        borderColor: '#83c7d4',
        borderWidth: 1,
        borderRadius: 5,

    },
    buttonsText: {
        marginLeft: 25,
    },
})
