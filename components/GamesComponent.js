import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'

const GamesComponent = ({item, joinGame, userToken}) => {
    return (
        <View testID={'wrapper'} style={styles.container}>
            <Text style={styles.title}>Game: <Text testID={'statusText'} style={styles.status}>{item.gameStatus}</Text></Text>
            {item.gameStatus !== 'OPEN' || item.playerToken === userToken ? null : 
            <TouchableOpacity testID={'gamesComponentButton'} style={styles.buttons} onPress={() => joinGame(item)}>
                <Text>Join</Text>
            </TouchableOpacity>
            }
        </View>
    )
}

export default GamesComponent

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        padding: 10,
        marginBottom: 3,
        borderBottomWidth: 1,
        borderColor: '#83c7d4',
    },
    buttons: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9cf0ff',
        borderColor: '#83c7d4',
        borderWidth: 1,
        borderRadius: 5,
        padding: 5,
    },
    title:{
        padding: 5,
        fontWeight: 'bold',
    }, 
    status: {
        fontWeight: 'normal'
    }
})
