import React from 'react'
import { StyleSheet, Text, View , TouchableOpacity} from 'react-native'
import { SimpleLineIcons } from '@expo/vector-icons';
import ButtonComponent from './ButtonComponent'
import { Foundation } from '@expo/vector-icons';
const GetResult = ({getResult, errorMessage}) => {
    return (
        <View testID={'wrapper'} style={styles.container}>
        {errorMessage? <Text testID={'textDisplayError'} style={styles.errorMessage}>{errorMessage}</Text>: null}
        <ButtonComponent
            title='Get Result'
            onPress={() => getResult()}
            icon={<Foundation name="results" size={24} color="black" />}
        />

    </View>
    )
}

export default GetResult

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconStyle: {
        fontSize: 20,
    }, 
    errorMessage: {
        color: 'red',
        padding: 10,
    },
})
