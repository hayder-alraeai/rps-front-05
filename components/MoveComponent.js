import React from 'react'
import { StyleSheet, Text, View , TouchableOpacity} from 'react-native'
import { FontAwesome } from '@expo/vector-icons';

const Move = ({setPlayerMove, errorMessage}) => {
    return (
        <>
            {errorMessage? <Text testID={'text'} style={styles.errorMessage}>{errorMessage}</Text>: null}
            <View testID={'wrapper'} style={styles.buttonsContainer}>
                <TouchableOpacity testID={'rock'} style={styles.buttons} onPress={() => setPlayerMove('ROCK')} >
                    <FontAwesome name="hand-rock-o" style={styles.iconStyle} />
                </TouchableOpacity>
                <TouchableOpacity testID={'scissors'} style={styles.buttons} onPress={() => setPlayerMove('SCISSORS')} >
                    <FontAwesome name="hand-scissors-o" style={styles.iconStyle} />
                </TouchableOpacity>
                <TouchableOpacity testID={'paper'} style={styles.buttons} onPress={() => setPlayerMove('PAPER')} >
                    <FontAwesome name="hand-paper-o" style={styles.iconStyle} />
                </TouchableOpacity>
            </View>
        </>
    )
}

export default Move

const styles = StyleSheet.create({
    buttonsContainer: {
        marginTop: 300,
        flexDirection: 'row',
    },
    buttons: {
        marginHorizontal: 5,
        width: '20%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9cf0ff',
        padding: 10,
        borderColor: '#83c7d4',
        borderWidth: 1,
        borderRadius: 5,

    },
    errorMessage: {
        color: 'red',
        padding: 10,
    },
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#121212',
        marginVertical: 100,
    },
    iconStyle: {
        fontSize: 30,
    },
})
