import { StatusBar } from 'expo-status-bar';
import React from 'react';
import Main from './stacks/Main';

export default function App() {
  return (
     <Main />
  );
}

