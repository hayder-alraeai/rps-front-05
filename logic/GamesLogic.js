import {gameCall} from '../api/ApiCalls'

   export const getGames = async (userToken, errorMessageHandler, setGames) => {
        try{
            const res = await gameCall(userToken, '/games')
            setGames(await res.data)

        }catch(e){
            errorMessageHandler(e.response.data.message)
        }

    }
    export const joinGame = async (userToken, game, errorMessageHandler, navigation) => {
        try{
            await gameCall(userToken, `/games/join/${game.id}`)
            navigation.navigate('Move', {game: game})
        }catch(e){
            errorMessageHandler(response.data.message)
        }
    }
    export const foo = (name) => {
        return name
    }