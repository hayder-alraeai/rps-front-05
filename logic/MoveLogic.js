import {gameCall} from '../api/ApiCalls'

export const setPlayerMove = async(value, userToken, setGameMoved, game, errorMessageHandler) => {
    console.log()
    try{
        const res = await  gameCall(userToken, `/games/move/${game.id}/${value}`)
        setGameMoved(await res.data)
        errorMessageHandler('')
    }catch(e){
        errorMessageHandler(e.response.data.message)
    }
}
export const getResult = async (userToken, game, setResult, errorMessageHandler) => {
    try{
        const res = await gameCall(userToken, `/games/result/${game.id}`)
        setResult(await res.data.result)
        errorMessageHandler('')
    }catch(e){
        errorMessageHandler(e.response.data.message)
    }
}