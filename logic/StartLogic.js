import {gameCall} from '../api/ApiCalls'

export const startGame = async(userToken, navigation, errorMessageHandler) => {
    try{
        const res = await gameCall(userToken,'/games/start');
        await navigation.navigate('Move', {game:res.data})
        errorMessageHandler('')
    }catch(e){
        errorMessageHandler(e.response.data.message)
    }     
}