import {render, cleanup, act} from 'react-native-testing-library'
import Login from '../../screens/Login'
import React from 'react'

afterEach(cleanup)

describe('<Login />', () => {
    it('should match the snapshot', () => {
        const rendered = render(<Login />).toJSON()
        expect(rendered).toMatchSnapshot()
    })
})
