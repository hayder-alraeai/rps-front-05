import {render, cleanup, act} from 'react-native-testing-library'
import Games from '../../screens/Games'
import React from 'react'
import ContextProvider from '../../Contex/ApplicationContext'

afterEach(cleanup)

test('test', async () => {
    const navigation = jest.fn()
    const provider = act(() => {
        <ContextProvider>
            <Games navigation={navigation} />
         </ContextProvider>
    })
    const element = render(
        <provider />
    ).toJSON()
    expect(element).toMatchSnapshot()
})