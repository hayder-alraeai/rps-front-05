import React from 'react'
import {render, cleanup, act, fireEvent} from 'react-native-testing-library'
import Start from '../../screens/Start'
import ContextProvider from '../../Contex/ApplicationContext'

afterEach(cleanup)

describe('<Start />', () => {
    it('Should match the snapshot', () => {
        const route = {params: {game: 'test'}}
        const acted = act(() => {
            <ContextProvider>
                <Start route={route} />
             </ContextProvider>
        })
        
        const rendered = render(<acted />)
        expect(rendered).toMatchSnapshot()
    })
})