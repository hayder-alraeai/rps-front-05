import Home from '../../screens/Home'
import React from 'react'
import {render, cleanup} from 'react-native-testing-library'

afterEach(cleanup)

describe('<Home />', () => {
    it('Should match snapshot', () => {
        const rendered = render(<Home navigation={'test navigation'} />).toJSON()
        expect(rendered).toMatchSnapshot()
    })
    it('View element should have 3 children to pass', () => {
        const rendered = render(<Home navigation={'test navigation'} />)
        const viewElement = rendered.getByTestId('wrapper')
        expect(viewElement.children.length).toBe(3)
    })
})



