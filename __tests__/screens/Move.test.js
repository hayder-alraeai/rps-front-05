import React from 'react'
import {render, cleanup, act} from 'react-native-testing-library'
import Move from '../../screens/Move'
import ContextProvider from '../../Contex/ApplicationContext'
afterEach(cleanup)

describe('<Move />', () => {
    it('Should match the snapshot', () => {
        const route = {params: {game: 'test'}}
        const acted = act(() => {
            <ContextProvider>
                <Move route={route} />
             </ContextProvider>
        })
        const rendered = render(<acted />)
        expect(rendered).toMatchSnapshot()
    })
})
