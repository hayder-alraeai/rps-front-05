import React from 'react'
import {render, cleanup, fireEvent} from 'react-native-testing-library'
import ButtonComponent from '../../components/ButtonComponent'

afterEach(cleanup)

describe('<ButtonComponent />', () => {
    
   it('Should match snapshot', () => {
        const onPress = jest.fn()
        const rendered = render(<ButtonComponent title='test' icon='test-icon' onPress={onPress} />).toJSON()
        expect(rendered).toMatchSnapshot()
    })
    it('Should fire when pressed', () => {
        const onPress = jest.fn()
        const rendered = render(<ButtonComponent title='test' icon='test-icon' onPress={onPress} />)
        const buttonComponent = rendered.getByTestId('buttonComponent')
        fireEvent(buttonComponent, 'press')
        expect(onPress).toHaveBeenCalled()
    })
    it('Should match the title props', () => {
        const onPress = jest.fn()
        const rendered = render(<ButtonComponent title='test' icon='test-icon' onPress={onPress} />)
        const buttonComponent = rendered.getByTestId('buttonTitle')
        expect(buttonComponent.props.children).toEqual('test')
    })

})

