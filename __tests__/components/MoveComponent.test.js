import React from 'react'
import {render, cleanup, fireEvent} from 'react-native-testing-library'
import MoveComponent from '../../components/MoveComponent'

afterEach(cleanup)

describe('<MoveComponent />', () => {
    it('Should match the snapshot', () => {
        const errorMessage = 'error'
        const setPlayerMove = jest.fn()
        const rendered = render(<MoveComponent setPlayerMove={setPlayerMove} errorMessage={errorMessage} />).toJSON()
        expect(rendered).toMatchSnapshot()
    })
    it('text element should render the error message if it not null', () => {
        const errorMessage = 'error'
        const setPlayerMove = jest.fn()
        const rendered = render(<MoveComponent setPlayerMove={setPlayerMove} errorMessage={errorMessage} />)
        const textElement = rendered.getByTestId('text')
        expect(textElement.props.children).toBe('error')
    })
    it('buttos wrapper should have 3 children', () => {
        const errorMessage = 'error'
        const setPlayerMove = jest.fn()
        const rendered = render(<MoveComponent setPlayerMove={setPlayerMove} errorMessage={errorMessage} />)
        const viewElement = rendered.getByTestId('wrapper')
        expect(viewElement.children.length).toBe(3)
    })
    it('buttos fire setPlayer rock function should pass', () => {
        const errorMessage = 'error'
        const setPlayerMove = jest.fn()
        const rendered = render(<MoveComponent setPlayerMove={setPlayerMove} errorMessage={errorMessage} />)
        const rockButton = rendered.getByTestId('rock')
        fireEvent(rockButton, 'press')
        expect(setPlayerMove).toHaveBeenCalled()
    })
    it('buttos fire setPlayer paper function should pass', () => {
        const errorMessage = 'error'
        const setPlayerMove = jest.fn()
        const rendered = render(<MoveComponent setPlayerMove={setPlayerMove} errorMessage={errorMessage} />)
        const rockButton = rendered.getByTestId('paper')
        fireEvent(rockButton, 'press')
        expect(setPlayerMove).toHaveBeenCalled()
    })
    it('buttos fire setPlayer scissors function should pass', () => {
        const errorMessage = 'error'
        const setPlayerMove = jest.fn()
        const rendered = render(<MoveComponent setPlayerMove={setPlayerMove} errorMessage={errorMessage} />)
        const rockButton = rendered.getByTestId('scissors')
        fireEvent(rockButton, 'press')
        expect(setPlayerMove).toHaveBeenCalled()
    })
})
