import React from 'react'
import {render, cleanup} from 'react-native-testing-library'
import GetResult from '../../components/GetResult'

afterEach(cleanup)

describe('<GetResult />', () => {
    it('should matches the snapshot', () => {
        const getResult = jest.fn()
        const errorMessage = 'error'
        const rendered = render(<GetResult getResult={getResult}  errorMessage={errorMessage}/>).toJSON()
        expect(rendered).toMatchSnapshot()
    })
    it('wrapper should have to children when error message is not null', () => {
        const getResult = jest.fn()
        const errorMessage = 'error'
        const rendered = render(<GetResult getResult={getResult}  errorMessage={errorMessage}/>)
        const viewElement = rendered.getByTestId('wrapper')
        expect(viewElement.children.length).toBe(2)
    })
    it('text element should render the error message when it is not null', () => {
        const getResult = jest.fn()
        const errorMessage = 'error'
        const rendered = render(<GetResult getResult={getResult}  errorMessage={errorMessage}/>)
        const textElement = rendered.getByTestId('textDisplayError')
        expect(textElement.props.children).toBe('error')
    })
})
