import React from 'react'
import {render, cleanup, fireEvent } from 'react-native-testing-library'
import GamesComponent from '../../components/GamesComponent'

afterEach(cleanup)

describe('<GamesComponent />', () => {
    it('Should matches the snapshot', () => {
        const joinGame = jest.fn()
        const rendered = render(<GamesComponent item={'item test'} joinGame={joinGame} userToken={'testToken'} />).toJSON()
        expect(rendered).toMatchSnapshot()
    })
    it('wrapper view element should have two children', () => {
        const joinGame = jest.fn()
        const item = {gameStatus: 'OPEN'}
        const rendered = render(<GamesComponent item={item} joinGame={joinGame} userToken={'testToken'} />)
        const viewElement = rendered.getByTestId('wrapper')
        expect(viewElement.children.length).toEqual(2)
    })
    it('text which render gameStatus should match the gameStatus prop', () => {
        const joinGame = jest.fn()
        const item = {gameStatus: 'OPEN'}
        const rendered = render(<GamesComponent item={item} joinGame={joinGame} userToken={'testToken'} />)
        const textElement = rendered.getByTestId('statusText')
        expect(textElement.props.children).toEqual('OPEN')
    })
    it('button  should fire when pressed', () => {
        const joinGame = jest.fn()
        const item = {gameStatus: 'OPEN'}
        const rendered = render(<GamesComponent item={item} joinGame={joinGame} userToken={'testToken'} />)
        const buttonElement = rendered.getByTestId('gamesComponentButton')
        fireEvent(buttonElement, 'press')
        expect(joinGame).toHaveBeenCalled()    
    })
})
