import React, {useState, useEffect} from 'react'
import { SimpleLineIcons } from '@expo/vector-icons';
import { StyleSheet, Text, View } from 'react-native'
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'
import { AsyncStorage } from 'react-native';
import {gameCall, getTokenCall} from '../api/ApiCalls'
import Login from '../screens/Login'
import Home from '../screens/Home'
import Games from '../screens/Games'
import Start from '../screens/Start'
import ContextProvider from '../Contex/ApplicationContext'
import Move from '../screens/Move'
import { TouchableOpacity } from 'react-native-gesture-handler'
const Main = () => {

const Stack = createStackNavigator()
    const [userToken, setUserToken] = useState('')
    const [errorMessage, setErrorMessage] = useState('')
    useEffect(() => {
        getToken();
    },[])

    const getToken = async() => {
        let token;
        try{
            token = await AsyncStorage.getItem('userToken')
            const res = await gameCall(userToken, '/auth/validate')
                res.data.message === 'OK'? setUserToken(token) : logout()
        }catch(e){
            console.log('cannot find token in local storage ' + e)
        }
    }
    const login = async () => {
        try{
           const res = await getTokenCall()
            await AsyncStorage.setItem('userToken', res.data)
            setUserToken(res.data)
            setErrorMessage('')
        }catch(e){
            setErrorMessage('Login faild:  ' + e)
        }
    }
    const logout = async () => {
        try{
            await AsyncStorage.removeItem('userToken')
            setUserToken('')
        }catch(e){
            console.log(e)
        }
    }

    if(!userToken){
        return(
            <NavigationContainer>
                <Stack.Navigator screenOptions={
                    {
                        headerStyle: {
                            backgroundColor: '#9cf0ff',
                            borderBottomWidth: 1,
                            borderColor: '#000',
                        }
                    }
                }>
                    <Stack.Screen name='Login' children={() => <Login login={login} errorMessage={errorMessage}/>} />
                </Stack.Navigator>
            </NavigationContainer>
        )
    }
    return (
        <ContextProvider>
        <NavigationContainer>
            <Stack.Navigator screenOptions={
                    {
                        headerStyle: {
                            backgroundColor: '#9cf0ff',
                            borderBottomWidth: 1,
                            borderColor: '#000',
                        },
                        headerRight: () => (
                            <TouchableOpacity style={styles.logoutButton} onPress={logout}>
                            <SimpleLineIcons style={styles.logoutIcon} name="logout" />
                            <Text style={styles.logoutText}>Logout</Text>
                        </TouchableOpacity>
                        )
                    }}>
                 <Stack.Screen name='Home' component={Home}  />
                 <Stack.Screen name='Start' component={Start} />
                 <Stack.Screen name='Games' component={Games} />
                 <Stack.Screen name='Move' component={Move} />
            </Stack.Navigator>
        </NavigationContainer>
        </ContextProvider>
    )
}

export default Main

const styles = StyleSheet.create({
    logoutButton: {
        marginVertical: 5,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#f76743',
        borderColor: '#83c7d4',
        borderWidth: 1,
        borderRadius: 5,
        marginRight: 10,
        padding: 5,
    },
    logoutText: {
        color: 'white',
        marginLeft: 5,
        fontWeight: 'bold',
    },
    logoutIcon: {
        color: 'white',
        fontSize: 15,
    }
})
