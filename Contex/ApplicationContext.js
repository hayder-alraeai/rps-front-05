import React, { useEffect, useState } from 'react'
import { AsyncStorage } from 'react-native';

export const context = React.createContext()
const ContextProvider = ({children}) => {
    const [userToken, setUserToken] = useState('')
    const [errorMessage, setErrorMessage] = useState('')

    useEffect(() => {
        getToken()
    }, [])

    const getToken = async() => {
        try{
            let token = await AsyncStorage.getItem('userToken')
            setUserToken(token)
        }catch(e){
            console.log(e)
        }
    }
    const errorMessageHandler = (value) => {
        setErrorMessage(value)
    }
    return(
        <context.Provider value={{userToken, errorMessage, errorMessageHandler}} >
            {children}
        </context.Provider>
    )
}
export default  ContextProvider;