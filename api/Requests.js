import axios from 'axios'

export default axios.create({
    baseURL: 'https://rps-05.herokuapp.com'
})