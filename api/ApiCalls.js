import axios from 'axios'

const req = axios.create({
    baseURL: 'https://rps-05.herokuapp.com'
})
export const getTokenCall = async () => {
        const res = await  req.get("/auth/token");
        return res

}
export const gameCall = async (userToken, path) => {
        const res = await  req.get(path, {headers: {Token : userToken}});
        return res
}
