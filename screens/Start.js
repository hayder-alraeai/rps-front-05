import React, {useContext} from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import {context} from '../Contex/ApplicationContext'
import {startGame} from '../logic/StartLogic'
import { SimpleLineIcons } from '@expo/vector-icons'; 
import ButtonComponent from '../components/ButtonComponent';

const Start = ({navigation}) => {
    const {userToken, errorMessage, errorMessageHandler} = useContext(context)

    return (
        <View style={styles.container}>
            {errorMessage? <Text style={styles.errorMessage}>{errorMessage}</Text>: null}
            
            <ButtonComponent 
                title= 'Start New Game'
                onPress={() => startGame(userToken, navigation, errorMessageHandler)}
                icon = {<SimpleLineIcons style={styles.iconStyle} name="control-start" />}
            />

        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    errorMessage: {
        color: 'red',
    },
    iconStyle: {
        fontSize: 20,
    },  
})
export default Start
