import React from 'react'
import { TouchableOpacity, StyleSheet, Text, View } from 'react-native'
import { SimpleLineIcons } from '@expo/vector-icons'; 
import ButtonComponent from '../components/ButtonComponent'
const Login = ({login, errorMessage}) => {

    return (
        <View style={styles.container}>
            {errorMessage? <Text style={styles.errorMessage}>{errorMessage}</Text> : null}
            <Text style={styles.title}>Get Token to be able to play</Text>
            <ButtonComponent 
                title='Get Token to play'
                onPress={login}
                icon = {<SimpleLineIcons name="login" style={styles.iconStyle} />}
            />
        </View>
    )
}

export default Login

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    errorMessage: {
        color: 'red',
        padding: 10,
    },
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#121212',
        marginVertical: 100,
    },
    iconStyle: {
        fontSize: 20,
    }, 
})
