import React, { useState, useContext, useEffect } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import {context} from '../Contex/ApplicationContext'
import MoveComponent from '../components/MoveComponent'
import GetResult from '../components/GetResult';
import {setPlayerMove, getResult} from '../logic/MoveLogic'
const Move = ({route}) => {
    const {game} = route.params
    const {userToken, errorMessage, errorMessageHandler} = useContext(context)
    const [getMove, setMove] = useState('')
    const [gameMoved , setGameMoved] = useState(null)
    const [result, setResult] = useState('')
    useEffect(() => {
        if(getMove){
            setPlayerMove(getMove, userToken, setGameMoved, game, errorMessageHandler)
        }
    },[getMove])
    if(result){
        return(
            <View styel={styles.container}>
                {errorMessage? <Text style={styles.errorMessage}>{errorMessage}</Text>: null}
                <Text style={styles.title}>{result}</Text>
            </View>
        )
    }
    return (
        <View style={styles.container}>
            {gameMoved ? 
                <GetResult 
                getResult={() => getResult(userToken, game, setResult, errorMessageHandler)}
                errorMessage={errorMessage}
                /> :
                <MoveComponent 
                setPlayerMove={setMove}
                errorMessage={errorMessage}
        />
        }
        </View>
    )
}

export default Move

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
    }, 
    title: {
        fontSize: 20,
        marginVertical: 300,
        alignSelf: 'center',
    }   
})
