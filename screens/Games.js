import React, { useContext, useEffect, useState } from 'react'
import { StyleSheet, Text, View , FlatList, Button} from 'react-native'
import {context} from '../Contex/ApplicationContext'
import {getGames, joinGame} from '../logic/GamesLogic'
import GamesComponent from '../components/GamesComponent'

const Games = ({navigation}) => {
    const {userToken, errorMessage, errorMessageHandler} = useContext(context)
    const [games, setGames] = useState([])
    useEffect(() => {
        getGames(userToken, errorMessageHandler, setGames)
        const listener = navigation.addListener('focus', () => {
            getGames(userToken, errorMessageHandler, setGames)
        })    
        return listener
    },[])

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Available Games</Text>
            {!games.length? 
                <View>{errorMessage? <Text style={styles.errorMessage}>{errorMessage}</Text>: 
                                    <Text style={styles.errorMessage}>No games</Text>}</View>:
                <FlatList 
                data={games}
                keyExtractor={item => item.id}
                renderItem= {({item}) => (
                    <GamesComponent 
                        item={item}
                        joinGame = {() => joinGame(userToken, item, errorMessageHandler, navigation)} 
                        userToken= {userToken}
                    />
                )}
            />
            }
            <Text style={styles.footer}></Text>
        </View>
    )
}

export default Games

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    title:{
        fontSize: 20,
        fontWeight: 'bold',
        alignSelf: 'center',
        marginVertical: 10,
    },
    footer: {
        bottom: 0,
        padding: 10,
    },
    errorMessage : {
        color: 'red',
        padding: 10,
    }
})
