import React from 'react'
import {StyleSheet, View, Image } from 'react-native'
import { AntDesign, SimpleLineIcons } from '@expo/vector-icons';
import ButtonComponent from '../components/ButtonComponent';
const Home = ({navigation}) => {
    return (
        <View testID={'wrapper'} style={styles.container}>

            <Image style={styles.logoImage} source={require('../assets/bg.png')} />
            <ButtonComponent
                onPress={() => navigation.navigate('Start')}
                title='Start new Game'
                icon = {<SimpleLineIcons style={styles.iconStyle} name="control-start" />}
            />
            <ButtonComponent 
                onPress={() => navigation.navigate('Games')}
                title= 'Available Games'
                icon = {<AntDesign name="fork" style={styles.iconStyle}/>}
            />
        </View>
    )
}

export default Home

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    logoImage: {
        width: 200,
        height: 200,
        marginVertical: 100,
    },
    errorMessage: {
        color: 'red',
    },
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#121212',
        marginVertical: 100,
    },
    iconStyle: {
        fontSize: 20,
    }, 

})
